//Alex Shah, Version 1.0, 12/2/15, CMPT 120-113

//item objects
var itemsArray = [
	new Item("skeleton key", "a key with a skull handle and inlaid jewels"),
	new Item("key", "a small brass key"),
	new Item("wooden plank", "a large, sturdy wooden plank"),
	new Item("unburied bones", "the bones of a poor lost soul")
];

//location objects
var locationsArray = [
	new Location("marsh", "A stinking marsh extends as far as the eye can see.", itemsArray[0]),
	new Location("lake", "A lake opens after a small drop from grass to sand to cool water.", null),
	new Location("boatHouse", "Old houses creak, and it seems old boathouses have a song of their own. The door requires a key to get in...", null),
	new Location("knottedTree", "This tree has a hole in the center, a knot where a sapling seems to have made a home. ", itemsArray[1]),
	new Location("openField", "You stand amongst tall grass, you can see woods on either side of this expansive open field.", itemsArray[2]),
	new Location("tallTree", "A tree stands tall. Taller than you thought possible. You would hurt your neck just to squint to see the top.", null),
	new Location("crow", "Amongst the trees is a black crow. The blackness of it's feathers seems to soak to it's eyes. Almost devoid of all light if it weren't for the glint of curiousness you see behind them.", null),
	new Location("toppledBike", "What was once a child's plaything, a blue and green bicycle lays on its side.", null),
	new Location("tireSwing", "Hanging from a tree is a tire swing, too heavy to swing in the breeze, but drips water from a recent rain.", null),
	new Location("graves", "A chilling graveyard. The wind howls, and sends the otherwise cheerful feeling of the area to the back of your mind.", itemsArray[3])
];

//location ctor
function Location(setName, setDescrip, setItem) {
    this.locName = setName;
    this.locDescrip = setDescrip;
    this.locItem = setItem;
    this.hasVisited = false;
}
//location toString prototype
Location.prototype.toString = function() {
	if (player.currentLocation === this) {
		return this.locDescrip;
	}
}
//item ctor
function Item(setName, setDescrip) {
    this.itemName = setName;
    this.itemDescrip = setDescrip;
    this.isFound = false;
    this.isTaken = false;
    this.isUsed = false;
}

//item toString prototype
Item.prototype.toString = function() {
	if (this.isFound === true) {
		return this.itemDescrip;
	}
}

//begin location functions
function goMarsh() {
    player.currentLocation = locationsArray[0];
    var message = locationsArray[0];
    if (locationsArray[0].hasVisited === false) {
        player.points += 5;
        locationsArray[0].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goLake() {
    player.currentLocation = locationsArray[1];
    var message = locationsArray[1];
    if (locationsArray[1].hasVisited === false) {
        player.points += 5;
        locationsArray[1].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goBoatHouse() {
    player.currentLocation = locationsArray[2];
    var message = locationsArray[2];
    if (locationsArray[2].hasVisited === false) {
        player.points += 5;
        locationsArray[2].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goKnottedTree() {
    player.currentLocation = locationsArray[3];
    var message = locationsArray[3];
    if (locationsArray[3].hasVisited === false) {
        player.points += 5;
        locationsArray[3].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goOpenField() {
    player.currentLocation = locationsArray[4];
    var message = locationsArray[4];
    if (locationsArray[4].hasVisited === false) {
        player.points += 5;
        locationsArray[4].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goTallTree() {
    player.currentLocation = locationsArray[5];
    var message = locationsArray[5];
    if (locationsArray[5].hasVisited === false) {
        player.points += 5;
        locationsArray[5].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goCrow() {
    player.currentLocation = locationsArray[6];
    var message = locationsArray[6];
    if (locationsArray[6].hasVisited === false) {
        player.points += 5;
        locationsArray[6].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goToppledBike() {
    player.currentLocation = locationsArray[7];
    var message = locationsArray[7];
    if (locationsArray[7].hasVisited === false) {
        player.points += 5;
        locationsArray[7].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goTireSwing() {
    player.currentLocation = locationsArray[8];
    var message = locationsArray[8];
    if (locationsArray[8].hasVisited === false) {
        player.points += 5;
        locationsArray[8].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

function goGraves() {
    player.currentLocation = locationsArray[9];
    var message = locationsArray[9];
    if (locationsArray[9].hasVisited === false) {
        player.points += 5;
        locationsArray[9].hasVisited = true;
    } else {
 		message = message + " You have already been here."
    }
    locationMessage(message);
    player.breadcrumbTrail.push(player.currentLocation.locName);
}

//end location functions

// Map
//                           graves
//crow        toppledBike    tireSwing
//knottedTree openField      tallTree
//marsh       lake           boatHouse
//
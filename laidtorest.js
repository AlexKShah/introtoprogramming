//Alex Shah, Version 1.0, 12/2/15, CMPT 120-113


//Set message for display
function locationMessage(msgInFunct) {
    document.getElementById("setScene").innerHTML = msgInFunct;
}

//Set points for display
function PointsText(pointsVar) {
    document.getElementById("pointsDisplay").innerHTML = pointsVar;
}

window.onload = function() {
	disableButtons();
}

//Holds player info
var player = {
    currentLocation: null,
    points: 0,
    inventory: [],
    breadcrumbTrail: []
};

//get command input, run function
function runCommand() {
    var inputbox = document.getElementById("command").value;
    if (inputbox.toLowerCase() === "n") {
        North();
    } else if (inputbox.toLowerCase() === "s") {
        South();
    } else if (inputbox.toLowerCase() === "e") {
        East();
    } else if (inputbox.toLowerCase() === "w") {
        West();
    } else if (inputbox.toLowerCase() === "t") {
        takeItem();
    } else if (inputbox.toLowerCase() === "i") {
        showInventory();
    } else if (inputbox.toLowerCase() === "h") {
        showHelpMenu();
    } else if (inputbox.toLowerCase() === "p") {
        showPreviousSteps();
    } else if (inputbox.toLowerCase() === "x") {
        examineLocation();
    } else if (inputbox.toLowerCase() === "u") {
        useItem();
    } else {
        locationMessage("Invalid command. Type h for help")
    }
}

//shows help
function showHelpMenu() {
    locationMessage("n goes North </br> s goes South </br> e goes East </br> w goes west </br> t takes item </br>"
        + "i shows inventory </br> h shows help </br> p shows previous steps </br>"
        + "x examines the location </br> u uses the item </br> Go submits the command.")
}

//shows previous steps
function showPreviousSteps() {
    message = "Your location history: </br>";
    var i;
    for (i = 0; i < player.breadcrumbTrail.length; i++) {
        message = message + player.breadcrumbTrail[i] + "</br>";
    }
    locationMessage(message);
}

//takes item
function takeItem() {
	var itemHere = player.currentLocation.locItem;
	if (itemHere && itemHere.isFound) {
		player.currentLocation.locItem = null;
		player.inventory.push(itemHere.itemName);
		locationMessage("You take the " + itemHere.itemName + "!");
	} else {
		locationMessage("Nothing to take!")
    }
}

//shows inventory
function showInventory() {
    message = "Your inventory contains: </br>";
    var i;
    for (i = 0; i < player.inventory.length; i++) {
        message = message + player.inventory[i] + "</br>";
    }
    locationMessage(message);
}

//examines location
function examineLocation() {
	var itemHere = player.currentLocation.locItem;
	if (itemHere && !itemHere.isFound) {
		itemHere.isFound = true;
		locationMessage("You find " + itemHere.itemDescrip);
	} else {
		locationMessage("The area proves fruitless to search further")
	}
}

//greys out buttons if you can't go further
function disableButtons() {
    var NorthButton = document.getElementById("NorthButton");
    var SouthButton = document.getElementById("SouthButton");
    var EastButton = document.getElementById("EastButton");
    var WestButton = document.getElementById("WestButton");

    switch (player.currentLocation) {
        case locationsArray[6]:
            NorthButton.disabled = true;
            SouthButton.disabled = false;
            EastButton.disabled = false;
            WestButton.disabled = true;
            break;
        case locationsArray[7]:
            NorthButton.disabled = true;
            SouthButton.disabled = false;
            EastButton.disabled = false;
            WestButton.disabled = false;
            break;
        case locationsArray[9]:
            NorthButton.disabled = true;
            SouthButton.disabled = false;
            EastButton.disabled = true;
            WestButton.disabled = true;
            break;
        case locationsArray[0]:
            NorthButton.disabled = false;
            SouthButton.disabled = true;
            EastButton.disabled = false;
            WestButton.disabled = true;
            break;
        case locationsArray[1]:
            NorthButton.disabled = false;
            SouthButton.disabled = true;
            EastButton.disabled = false;
            WestButton.disabled = false;
            break;
        case locationsArray[2]:
            NorthButton.disabled = false;
            SouthButton.disabled = true;
            EastButton.disabled = true;
            WestButton.disabled = false;
            break;
        case locationsArray[8]:
            NorthButton.disabled = false;
            SouthButton.disabled = false;
            EastButton.disabled = true;
            WestButton.disabled = false;
            break;
        case locationsArray[5]:
            NorthButton.disabled = false;
            SouthButton.disabled = false;
            EastButton.disabled = true;
            WestButton.disabled = false;
            break;
        case locationsArray[3]:
            NorthButton.disabled = false;
            SouthButton.disabled = false;
            EastButton.disabled = false;
            WestButton.disabled = true;
            break;
        default:
            NorthButton.disabled = false;
            SouthButton.disabled = false;
            EastButton.disabled = false;
            WestButton.disabled = false;
            break;
    }
}

//North
function North() {
    switch (player.currentLocation) {
        case locationsArray[0]:
            goKnottedTree();
            break;
        case locationsArray[3]:
            goCrow();
            break;
        case locationsArray[1]:
            goOpenField();
            break;
        case locationsArray[4]:
            goToppledBike();
            break;
        case locationsArray[2]:
            goTallTree();
            break;
        case locationsArray[5]:
            goTireSwing();
            break;
        case locationsArray[8]:
            goGraves();
            break;
        case null:
            goOpenField();
            break;
    }
    PointsText(player.points);
    disableButtons();
}

//South
function South() {
    switch (player.currentLocation) {
        case locationsArray[6]:
            goKnottedTree();
            break;
        case locationsArray[3]:
            goMarsh();
            break;
        case locationsArray[7]:
            goOpenField();
            break;
        case  locationsArray[4]:
            goLake();
            break;
        case locationsArray[9]:
            goTireSwing();
            break;
        case locationsArray[8]:
            goTallTree();
            break;
        case locationsArray[5]:
            goBoatHouse();
            break;
        case null:
            goOpenField();
            break;
    }
    PointsText(player.points);
    disableButtons();;
}

//West
function West() {
    switch (player.currentLocation) {
        case locationsArray[8]:
            goToppledBike();
            break;
        case locationsArray[7]:
            goCrow();
            break;
        case locationsArray[5]:
            goOpenField();
            break;
        case locationsArray[4]:
            goKnottedTree();
            break;
        case locationsArray[2]:
            goLake();
            break;
        case locationsArray[1]:
            goMarsh();
            break;
        case null:
            goOpenField();
            break;
    }
    PointsText(player.points);
    disableButtons();
}

//East
function East() {
    switch (player.currentLocation) {
        case locationsArray[6]:
            goToppledBike();
            break;
        case locationsArray[7]:
            goTireSwing();
            break;
        case locationsArray[3]:
            goOpenField();
            break;
        case locationsArray[4]:
            goTallTree();
            break;
        case locationsArray[0]:
            goLake();
            break;
        case locationsArray[1]:
            goBoatHouse();
            break;
        case null:
            goOpenField();
            break;
    }
    PointsText(player.points);
    disableButtons();
}
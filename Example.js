//Assorted location variables to test if visited, 0 is no, 123 is yes
var t11 = 0;
var t12 = 0;
var t13 = 0;
//etc

//Test if variable is 123 otherwise give points
//then set txx to 123 which should set t11 to 123
function testBeen(txx){
    if (txx === 123){
      hasBeen();
    } else {
      //notBeen(); a function giving points and showing messages
      txx = settxx(txx);
    }
    return txx;
}

function settxx(txx) {
  txx = 123;
  return txx;
}

function hasBeen() {
    console.log("Original message" + "You've already been here");
}

//first visit
testBeen(t11);
//second visit
testBeen(t11);
